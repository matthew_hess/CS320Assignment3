#!/bin/bash

echo "Assignment #3-3, Matthew Hess, Matthew9510@gmail.com"
file1Contents=`cat memo.enc | ./prog3_3 $1`
file2Contents="$(cat production.enc | ./prog3_3 $1)"
file3Contents="$(cat recipe.enc | ./prog3_3 $1)"

echo $file1Contents > memo.dec
echo $file2Contents > production.dec
echo $file3Contents > recipe.dec

ls *.dec | sed 's/ /\n/g'
