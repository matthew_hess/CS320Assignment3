# CSAssignment3
## Created by: Matthew Hess
---
### Project Description:
+ This project will obtain, break, and decode some files from Terrible Tribbles inc.
+ This project reminded me that writing scripts gives us the ability to use multiple more focused programs to create a custom program to do multiple tasks.

---
### prog3_1.sh
   This program is written in bash script. In order to run it, you will need to open up your nearest terminal and jump in the directory where prog2_1.sh and the Login files are located.
   Then Run the program by typing the following line of code: `./prog3_1.sh Logins sd.lindeneau.com firstName`

   This Program:
  * Obtain Yoko's password from the login file, then copies all of the files in Yoko's directory. It also displays the files copies each on new lines.

---
### prog3_2.c
   This program is written in c. This program compares two different characters and outputs the integer difference.

### prog3_2.sh

   This program is written in bash. In order to run it, you will need to open up your nearest terminal and jump in the directory where prog3_2.sh is located and then run the program by typing the following line of code: `./prog3_2.sh`

   This Program:
  * calls the executable encryptor program with a preset character and then pushes that output into the prog3_2.c program to print out this particular caesars cipher key.

---

### prog3_3.c
   This program is written in c. This program implements the caesars cipher. It uses the prog3_2.sh output as a key to convert the encoded message to become a decoded phrase by manipulating the ascii values.
### prog3_3.sh
   This program is written in bash script. In order to run it, you will need to open up your nearest terminal and jump in the directory where prog3_3.sh
   
   Then
  * Run the program by typing the following line of code: `./prog3_3.sh`

   This Program:
  * Saves the cracked message of each .enc file in the directory and saves each files as a .dec filetype.
  * Then prints out all .dec filenames on separate lines 
