#/!/bin/bash

logins=$1
targetServer=$2
name=$3

echo "Assignment #3-1, Matthew Hess, matthew9510@gmail.com"
lowerFirst=`echo $name | cut -c1-4 | tr '[:upper:]' '[:lower:]'`
password=`grep -i "$name" $logins | cut -d "," -f3`
directory=/home/$lowerFirst/

expect -c "
spawn scp $lowerFirst@$targetServer:$directory* .
expect password: { send $password\r }
sleep 1
exit
" > /dev/null

ls *.enc encryptor | sed 's/ /\n/g'
