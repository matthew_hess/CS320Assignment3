#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

int main(int argc, char *argv[])
{
  int key = atoi(argv[1]);
  char phrase[256];
  char translated;
  char output;
  fgets(phrase, 256, stdin);

for(int i = 0; i< strlen(phrase); i++)
{
if(!isalpha(phrase[i])){
  printf("%c", phrase[i]); // handle spaces
}
else if (islower(phrase[i]) != 0){
  translated=phrase[i]-97;
  translated+= key;
  while (translated < 0) {
    translated += 26;
  }
  printf("%c", translated % 26 + 97);
}
else
{
  translated=phrase[i]-65;
  translated+= key;
  while(translated < 0) {
    translated += 26;
  }
  printf("%c", translated % 26 + 65);
}
}
printf("\n");
return 0;
}
